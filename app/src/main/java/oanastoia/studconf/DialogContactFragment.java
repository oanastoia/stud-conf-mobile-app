package oanastoia.studconf;

/**
 * Created by oanastoia on 5/4/15.
 */

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import oanastoia.studconf.adapter.SessionsListAdapter;
import oanastoia.studconf.model.SessionItem;
import oanastoia.studconf.model.UserItem;

public class DialogContactFragment extends DialogFragment  {

    private int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle args = getArguments();
        position = args.getInt("position");
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_contact, new LinearLayout(getActivity()), false);

        // get list of users
        List<UserItem> userItemList = ((MainActivity) getActivity()).getSavedContacts();
        // update view

        ((TextView) view.findViewById(R.id.contactPopupBadge)).setText(userItemList.get(position).getInitials());
        ((TextView) view.findViewById(R.id.participantHeader)).setText(userItemList.get(position).getFullName());
        ((TextView) view.findViewById(R.id.participantJobTitle)).setText(userItemList.get(position).getJobtitle());
        ((TextView) view.findViewById(R.id.participantEmail)).setText(userItemList.get(position).getEmail());
        ((TextView) view.findViewById(R.id.participantCompany)).setText(userItemList.get(position).getCompany());

        ArrayList<SessionItem> presenterSessions = ((MainActivity) getActivity()).getPresenterSessions(userItemList.get(position).getUserid());
        ListView mainListView = (ListView) view.findViewById(R.id.presenterSessionsList);
        SessionsListAdapter listAdapter = new SessionsListAdapter(getActivity().getApplicationContext(), presenterSessions);
        // add my details to the record list on screen
        mainListView.setAdapter(listAdapter);

        view.findViewById(R.id.buttonDeleteContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // trigger save event
                ((MainActivity) getActivity()).getSocketHandler().deleteContact(((MainActivity) getActivity()).getSavedContacts().get(position).getUserid());
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                DialogContactFragment dialog = (DialogContactFragment) fm.findFragmentByTag("contactDialog");
                Toast.makeText(getActivity(), "Contact " + ((MainActivity) getActivity()).getSavedContacts().get(position).getFullName() + " deleted ...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                DialogContactFragment dialog = (DialogContactFragment) fm.findFragmentByTag("contactDialog");
                dialog.dismiss();
            }
        });

        // Build dialog
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        builder.setContentView(view);
        return builder;
    }
}