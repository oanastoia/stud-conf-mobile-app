package oanastoia.studconf;

/**
 * Created by oanastoia on 5/4/15.
 */
import android.app.Dialog;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import oanastoia.studconf.model.SessionItem;

public class DialogSessionFragment extends DialogFragment  {

    protected SessionItem sessionItem;
    protected boolean isPinnedEvent = false;

    public DialogSessionFragment(SessionItem sessionItem, boolean isPinnedEvent)
    {
        this.sessionItem = sessionItem;
        this.isPinnedEvent = isPinnedEvent;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_calendar, new LinearLayout(getActivity()), false);

        if (isPinnedEvent) {
            view.findViewById(R.id.buttonPinSession).setVisibility(Button.INVISIBLE);
            view.findViewById(R.id.buttonUnpinSession).setVisibility(Button.VISIBLE);
        }
        else {
            if (!((MainActivity)getActivity()).getPreferences().isAppActivated()) {
                view.findViewById(R.id.buttonPinSession).setVisibility(Button.GONE);
            }
        }

        // Retrieve layout elements
        TextView title = (TextView) view.findViewById(R.id.eventTitleText);
        TextView presenter = (TextView) view.findViewById(R.id.eventPresenterText);
        TextView description = (TextView) view.findViewById(R.id.eventDescriptionText);

        // Set values
        title.setText(sessionItem.getName());
        description.setText(sessionItem.getDescription());
        presenter.setText("by " + sessionItem.getPresenterText());


        view.findViewById(R.id.buttonPinSession).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Popup", "User tried to save the event");
                // trigger save event
                ((MainActivity) getActivity()).getSocketHandler().pinSession(sessionItem.getSessionId());
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                DialogSessionFragment dialog = (DialogSessionFragment) fm.findFragmentByTag("eventDetailsDialog");
                Toast.makeText(getActivity(), "Session " + sessionItem.getName() + " added to my saved sessions ...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.buttonUnpinSession).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Popup", "User tried to unpin a session");
                ((MainActivity) getActivity()).getSocketHandler().unpinSession(sessionItem.getSessionId());
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                DialogSessionFragment dialog = (DialogSessionFragment) fm.findFragmentByTag("eventDetailsDialog");
                Toast.makeText(getActivity(), "Session " + sessionItem.getName() + " unpinned ...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        view.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                DialogSessionFragment dialog = (DialogSessionFragment) fm.findFragmentByTag("eventDetailsDialog");
                dialog.dismiss();
            }
        });

        // Build dialog
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        builder.setContentView(view);
        return builder;
    }
}