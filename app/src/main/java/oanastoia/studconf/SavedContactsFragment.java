package oanastoia.studconf;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import oanastoia.studconf.adapter.ContactListAdapter;
import oanastoia.studconf.model.UserItem;
import oanastoia.studconf.util.SocketHandler;

public class SavedContactsFragment extends Fragment {

	public SavedContactsFragment(){}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
        socketHandler.retrieveSavedContacts();
        View rootView = inflater.inflate(R.layout.fragment_saved_contacts, container, false);

        ArrayList<UserItem> savedContacts = ((MainActivity) getActivity()).getSavedContacts();
        ListView mainListView = (ListView) rootView.findViewById(R.id.saveContactsListView);
        ContactListAdapter listAdapter = new ContactListAdapter(getActivity().getApplicationContext(), savedContacts);
        // add my details to the record list on screen
        mainListView.setAdapter(listAdapter);
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentManager fm = getActivity().getFragmentManager();
                DialogContactFragment dialog = new DialogContactFragment();
                Bundle args = new Bundle();
                args.putInt("position", position);
                dialog.setArguments(args);
                dialog.show(fm, "contactDialog");
            }
        });


        return rootView;
    }
}
