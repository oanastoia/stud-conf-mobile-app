package oanastoia.studconf;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import oanastoia.studconf.model.AdminPreferences;
import oanastoia.studconf.util.SocketHandler;

public class AdminUsersFragment extends Fragment {

	public AdminUsersFragment(){}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
        if (((MainActivity) getActivity()).getAdminPreferences().isAuthenticated()) {
            socketHandler.retrieveUsers(((MainActivity) getActivity()).getAdminPreferences().getToken());
            socketHandler.retrieveWifiDetails(((MainActivity) getActivity()).getAdminPreferences().getToken());
        }
        View rootView = inflater.inflate(R.layout.fragment_admin_users, container, false);
        rootView.findViewById(R.id.loginButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
                String username = ((TextView) getActivity().findViewById(R.id.editTextUsername)).getText().toString();
                String password = ((TextView) getActivity().findViewById(R.id.editTextPassword)).getText().toString();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow((getActivity().findViewById(R.id.editTextPassword)).getWindowToken(), 0);
                socketHandler.login(username, password);
            }
        });

        rootView.findViewById(R.id.logoutButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send logout on socket
                SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
                socketHandler.logout(((MainActivity) getActivity()).getAdminPreferences().getToken());

                AdminPreferences preferences = ((MainActivity) getActivity()).getAdminPreferences();
                preferences.setToken("");
                ((MainActivity) getActivity()).writeAdminPreferences(preferences);
                ((MainActivity) getActivity()).switchViewsToNotAuth();
                ((MainActivity) getActivity()).setRedrawNeeded(true);
                Log.d("PREF", "Token erased from device");
            }
        });

        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (((MainActivity)getActivity()).getAdminPreferences().isAuthenticated()) {
            ((MainActivity)getActivity()).switchViewsToAuth();
        }
        else {
            ((MainActivity)getActivity()).switchViewsToNotAuth();
        }
    }
}
