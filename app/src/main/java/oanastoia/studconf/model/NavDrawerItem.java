package oanastoia.studconf.model;

public class NavDrawerItem {
	
	private String title;
	private int icon;
	private boolean isEnabled = true;

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public NavDrawerItem(){}

	public NavDrawerItem(String title, int icon){
		this.title = title;
		this.icon = icon;
	}

	public NavDrawerItem(String title, int icon, boolean isEnabled){
		this.title = title;
		this.icon = icon;
		this.isEnabled = isEnabled;
	}
	
	public String getTitle(){
		return this.title;
	}
	
	public int getIcon(){
		return this.icon;
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
	public void setIcon(int icon){
		this.icon = icon;
	}
}
