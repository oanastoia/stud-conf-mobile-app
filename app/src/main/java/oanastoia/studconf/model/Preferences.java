package oanastoia.studconf.model;

import android.content.SharedPreferences;

/**
 * Created by oanastoia on 21/05/15.
 */
public class Preferences {
    private String networkSSID;
    private String networkPass;
    private String serverHost = "52.10.204.133";
    private String serverPort = "8080";
    private UserItem userItem;

    private boolean nfcAvailable = true;

    public boolean isNfcAvailable() {
        return nfcAvailable;
    }

    public void setNfcAvailable(boolean nfcAvailable) {
        this.nfcAvailable = nfcAvailable;
    }

    public String getServerHost() {
        return serverHost;
    }

    public String getServerPort() {
        return serverPort;
    }

    public Preferences() {
        this.userItem = new UserItem();
        this.networkSSID = "";
        this.networkPass = "";
    }

    public Preferences(UserItem userItem, String networkSSID, String networkPass) {
        this.userItem = userItem;
        this.networkSSID = networkSSID;
        this.networkPass = networkPass;
    }

    public UserItem getUserItem() {
        return userItem;
    }

    public void setUserItem(UserItem userItem) {
        this.userItem = userItem;
    }

    public String getNetworkSSID() {
        return networkSSID;
    }

    public void setNetworkSSID(String networkSSID) {
        this.networkSSID = networkSSID;
    }

    public String getNetworkPass() {
        return networkPass;
    }

    public void setNetworkPass(String networkPass) {
        this.networkPass = networkPass;
    }

    public void savePreferences(SharedPreferences.Editor editor) {
        editor.putString("fistname", getUserItem().getFirstname());
        editor.putString("lastname", getUserItem().getLastname());
        editor.putString("company", getUserItem().getCompany());
        editor.putString("email", getUserItem().getEmail());
        editor.putString("jobtitle", getUserItem().getJobtitle());
        editor.putInt("userid", getUserItem().getUserid());
        editor.putString("networkpass", getNetworkPass());
        editor.putString("networkssid", getNetworkSSID());
        editor.commit();
    }

    public void loadPreferences(SharedPreferences settings) {
        getUserItem().setFirstname(settings.getString("fistname", ""));
        getUserItem().setLastname(settings.getString("lastname", ""));
        getUserItem().setCompany(settings.getString("company", ""));
        getUserItem().setEmail(settings.getString("email", ""));
        getUserItem().setJobtitle(settings.getString("jobtitle", ""));
        getUserItem().setUserid(settings.getInt("userid", 0));
        setNetworkPass(settings.getString("networkpass", ""));
        setNetworkSSID(settings.getString("networkssid", ""));
    }

    public boolean isAppActivated() {
        return getUserItem().getUserid() != 0;
    }
}
