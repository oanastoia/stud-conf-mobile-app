package oanastoia.studconf.model;

/**
 * Created by oanastoia on 21/05/15.
 */
public class UserItem {
    protected int userid;
    protected String jobtitle;
    protected String firstname;
    protected String lastname;
    protected String email;
    protected String company;
    protected String ispresenter;

    public String getIspresenter() {
        return ispresenter;
    }

    public void setIspresenter(String ispresenter) {
        this.ispresenter = ispresenter;
    }

    public int getUserid() {
        return userid;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getCompany() {
        return company;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public UserItem(int userid, String jobtitle, String firstname, String lastname, String email, String company, String ispresenter) {
        this.userid = userid;
        this.jobtitle = jobtitle;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
        this.ispresenter = ispresenter;
    }

    public UserItem() {
        this.userid = 0;
        this.jobtitle = "";
        this.firstname = "";
        this.lastname = "";
        this.email = "";
        this.company = "";
        this.ispresenter = "";
    }

    public boolean isPresenter() {
        return getIspresenter().equals("1");
    }

    public String getFullName() {
        return getFirstname().concat(" ").concat(getLastname());
    }

    public String getContactsText() {

        if (getLastname().equals("")) {
            return getFirstname().concat("\n").concat(getEmail());
        }
        else {
            return getFirstname().concat(" ").concat(getLastname()).concat("\n").concat(getEmail());
        }
    }

    public String getInitials() {
        if (getLastname().equals("")) {
            return getFirstname().substring(0, 1).toUpperCase();
        } else {
            return (getLastname().substring(0, 1)).concat(getFirstname().substring(0, 1)).toUpperCase();
        }
    }
}
