package oanastoia.studconf.model;

import android.content.SharedPreferences;

/**
 * Created by oanastoia on 21/05/15.
 */
public class AdminPreferences {
    public void setNetworkSSID(String networkSSID) {
        this.networkSSID = networkSSID;
    }

    public void setNetworkPass(String networkPass) {
        this.networkPass = networkPass;
    }

    private String networkSSID;
    private String networkPass;
    private String token;
    private boolean nfcAvailable = true;

    public boolean isNfcAvailable() {
        return nfcAvailable;
    }

    public void setNfcAvailable(boolean nfcAvailable) {
        this.nfcAvailable = nfcAvailable;
    }

    public String getNetworkSSID() {
        return networkSSID;
    }

    public String getNetworkPass() {
        return networkPass;
    }

    public AdminPreferences() {
        this.token = "";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isAuthenticated() {
        return !getToken().equals("");
    }

    public void loadPreferences(SharedPreferences settings) {
        // "" is just the default when no preferences found on the phone
        setToken(settings.getString("token", ""));
        setNetworkPass(settings.getString("adminNetworkPass", ""));
        setNetworkSSID(settings.getString("adminNetworkSSID", ""));
    }

    public void savePreferences(SharedPreferences.Editor editor) {
        editor.putString("token", getToken());
        editor.putString("adminNetworkPass", getNetworkPass());
        editor.putString("adminNetworkSSID", getNetworkSSID());
        editor.commit();
    }
}
