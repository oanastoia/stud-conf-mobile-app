package oanastoia.studconf.model;

import android.provider.BaseColumns;

/**
 * Created by oanastoia on 06/06/15.
 */
public final class StudConfContract {
    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public StudConfContract() {}

    /* Inner class that defines the table contents */
    public static abstract class SessionEntry implements BaseColumns {
        public static final String PINNED_TABLE_NAME = "pinnedsessions";
        public static final String TABLE_NAME = "calendar";
        public static final String COLUMN_NAME_STARTTIME = "STARTTIME";
        public static final String COLUMN_NAME_ENDTIME = "ENDTIME";
        public static final String COLUMN_NAME_SESSIONNAME = "SESSIONNAME";
        public static final String COLUMN_NAME_CATEGORYID = "CATEGORYID";
        public static final String COLUMN_NAME_SESSIONID = "SESSIONID";
        public static final String COLUMN_NAME_DESCRIPTION = "DESCRIPTION";
        public static final String COLUMN_NAME_PRESENTERID = "PRESENTERID";
        public static final String COLUMN_NAME_COLOR = "COLOR";
        public static final String COLUMN_NAME_NAME = "NAME";
        public static final String COLUMN_NAME_SURNAME = "SURNAME";

    }

    public static abstract class ContactEntry implements BaseColumns {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_NAME_JOBTITLE = "JOBTITLE";
        public static final String COLUMN_NAME_NAME = "NAME";
        public static final String COLUMN_NAME_SURNAME = "SURNAME";
        public static final String COLUMN_NAME_EMAIL = "EMAIL";
        public static final String COLUMN_NAME_COMPANY = "COMPANY";
        public static final String COLUMN_NAME_ISPRESENTER = "ISPRESENTER";
        public static final String COLUMN_NAME_USERID = "USERID";
    }
}
