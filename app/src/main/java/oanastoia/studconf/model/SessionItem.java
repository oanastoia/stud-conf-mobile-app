package oanastoia.studconf.model;

import com.alamkanak.weekview.WeekViewEvent;

import java.util.Calendar;

/**
 * Created by oanastoia on 04/05/15.
 */
public class SessionItem extends WeekViewEvent {
    private String mDescription;
    private int sessionId;
    private int presenterId;
    private String presenterName;
    private String presenterSurname;
    private int categoryId;

    public int getPresenterId() {
        return presenterId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getSessionId() {
        return sessionId;
    }

    public SessionItem(long id, String name, String description, Calendar startTime, Calendar endTime, int color, int sessionId, int presenterId, int categoryId, String presenterName, String presenterSurname) {
        super(id, name, startTime, endTime);
        this.mDescription = description;
        this.sessionId = sessionId;
        this.presenterId = presenterId;
        this.categoryId = categoryId;
        this.presenterName = presenterName;
        this.presenterSurname = presenterSurname;
        setColor(color);
    }

    public String getPresenterText() {
        return presenterName + " " + presenterSurname;
    }

    public String getPresenterName() {
        return presenterName;
    }

    public String getPresenterSurname() {
        return presenterSurname;
    }
    public String getDescription() {
        return mDescription;
    }
}
