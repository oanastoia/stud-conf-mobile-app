package oanastoia.studconf;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.alamkanak.weekview.WeekView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import oanastoia.studconf.adapter.NavDrawerListAdapter;
import oanastoia.studconf.model.AdminPreferences;
import oanastoia.studconf.model.NavDrawerItem;
import oanastoia.studconf.model.Preferences;
import oanastoia.studconf.model.SessionItem;
import oanastoia.studconf.model.StudConfContract;
import oanastoia.studconf.model.UserItem;
import oanastoia.studconf.util.NfcHandler;
import oanastoia.studconf.util.SocketHandler;
import oanastoia.studconf.util.StudConfDBHelper;
import oanastoia.studconf.util.WifiHandler;

public class MainActivity extends AppCompatActivity {

    public MenuItem socketStatusItem;
    protected NfcHandler nfcHandler;
    protected SocketHandler socketHandler;
    protected List<SessionItem> events;
    protected List<SessionItem> savedEvents;
    protected ArrayList<UserItem> savedContacts;
    protected ArrayList<UserItem> participantViewList;
    private StudConfDBHelper dbHelper;
    private boolean redrawNeeded = false;
    private String PREFS_NAME = "StudConfPref";
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private WifiHandler wifiHandler;
    private Preferences preferences;
    private AdminPreferences adminPreferences;

    public StudConfDBHelper getDbHelper() {
        return dbHelper;
    }

    public ArrayList<SessionItem> getPresenterSessions(int presenterId) {
        ArrayList<SessionItem> sessions = new ArrayList<SessionItem>();

        for (int i = 0; i < events.size(); i++) {
            if (events.get(i).getPresenterId() == presenterId) {
                sessions.add(events.get(i));
            }
        }
        return sessions;
    }

    public boolean isRedrawNeeded() {
        return redrawNeeded;
    }

    public void setRedrawNeeded(boolean redrawNeeded) {
        this.redrawNeeded = redrawNeeded;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public AdminPreferences getAdminPreferences() {
        return adminPreferences;
    }

    public void setAdminPreferences(AdminPreferences adminPreferences) {
        this.adminPreferences = adminPreferences;
    }

    public ArrayList<UserItem> getParticipantViewList() {
        return participantViewList;
    }

    public void setParticipantViewList(ArrayList<UserItem> participantViewList) {
        this.participantViewList = participantViewList;
    }

    public ArrayList<UserItem> getSavedContacts() {
        return savedContacts;
    }

    public void setSavedContacts(ArrayList<UserItem> savedContacts) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // clear saved data
        dbHelper.emptyContacts(db);
        // insert new sessions
        for (int i = 0; i < savedContacts.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_NAME, savedContacts.get(i).getFirstname());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_SURNAME, savedContacts.get(i).getLastname());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_JOBTITLE, savedContacts.get(i).getJobtitle());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_COMPANY, savedContacts.get(i).getCompany());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_EMAIL, savedContacts.get(i).getEmail());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_USERID, savedContacts.get(i).getUserid());
            values.put(StudConfContract.ContactEntry.COLUMN_NAME_ISPRESENTER, savedContacts.get(i).getIspresenter());
            long newRowId = db.insert(StudConfContract.ContactEntry.TABLE_NAME, "null", values);
        }
        this.savedContacts = savedContacts;
    }

    public List<SessionItem> getEvents() {
        // check if connection is down
        // query sql and return events
        return events;
    }

    public void setEvents(List<SessionItem> events) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // clear saved data
        dbHelper.emptySessions(db);
        // insert new sessions
        for (int i = 0; i < events.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME, events.get(i).getStartTime().getTimeInMillis());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME, events.get(i).getEndTime().getTimeInMillis());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME, events.get(i).getName());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID, events.get(i).getCategoryId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID, events.get(i).getSessionId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_COLOR, events.get(i).getColor());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION, events.get(i).getDescription());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID, events.get(i).getPresenterId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_NAME, events.get(i).getPresenterName());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SURNAME, events.get(i).getPresenterSurname());
            long newRowId = db.insert(StudConfContract.SessionEntry.TABLE_NAME, "null", values);
        }
        this.events = events;
    }

    public void injectPinnedSessions() {
        ArrayList<SessionItem> events = new ArrayList<SessionItem>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME,
                StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME,
                StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME,
                StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID,
                StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID,
                StudConfContract.SessionEntry.COLUMN_NAME_COLOR,
                StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION,
                StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID,
                StudConfContract.SessionEntry.COLUMN_NAME_NAME,
                StudConfContract.SessionEntry.COLUMN_NAME_SURNAME
        };
        String sortOrder =
                StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME + " ASC";

        String[] selectionArgs = new String[0];

        Cursor cursor = db.query(
                StudConfContract.SessionEntry.PINNED_TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                "",                                          // The columns for the WHERE clause
                selectionArgs,                              // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    SessionItem event = null;
                    Calendar startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    startTime.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME)));
                    Calendar endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    endTime.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME)));

                    event = new SessionItem(cursor.getPosition(),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION)),
                            startTime,
                            endTime,
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_COLOR)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_NAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SURNAME))
                    );
                    if (event != null) {
                        events.add(event);
                    }
                } while (cursor.moveToNext());
            }
        }
        this.savedEvents = events;
    }

    public void injectSessions() {
        ArrayList<SessionItem> events = new ArrayList<SessionItem>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME,
                StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME,
                StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME,
                StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID,
                StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID,
                StudConfContract.SessionEntry.COLUMN_NAME_COLOR,
                StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION,
                StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID,
                StudConfContract.SessionEntry.COLUMN_NAME_NAME,
                StudConfContract.SessionEntry.COLUMN_NAME_SURNAME
        };
        String sortOrder =
                StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME + " ASC";

        String[] selectionArgs = new String[0];

        Cursor cursor = db.query(
                StudConfContract.SessionEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                "",                                          // The columns for the WHERE clause
                selectionArgs,                              // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    SessionItem event = null;
                    Calendar startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    startTime.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME)));
                    Calendar endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    endTime.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME)));

                    event = new SessionItem(cursor.getPosition(),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION)),
                            startTime,
                            endTime,
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_COLOR)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID)),
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_NAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.SessionEntry.COLUMN_NAME_SURNAME))
                    );
                    if (event != null) {
                        events.add(event);
                    }
                } while (cursor.moveToNext());
            }
        }
        this.events = events;
    }

    public void injectContacts() {
        ArrayList<UserItem> contacts = new ArrayList<UserItem>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                StudConfContract.ContactEntry.COLUMN_NAME_NAME,
                StudConfContract.ContactEntry.COLUMN_NAME_SURNAME,
                StudConfContract.ContactEntry.COLUMN_NAME_JOBTITLE,
                StudConfContract.ContactEntry.COLUMN_NAME_COMPANY,
                StudConfContract.ContactEntry.COLUMN_NAME_EMAIL,
                StudConfContract.ContactEntry.COLUMN_NAME_USERID,
                StudConfContract.ContactEntry.COLUMN_NAME_ISPRESENTER
        };
        String sortOrder =
                StudConfContract.ContactEntry.COLUMN_NAME_SURNAME + " ASC";

        String[] selectionArgs = new String[0];

        Cursor cursor = db.query(
                StudConfContract.ContactEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                "",                                          // The columns for the WHERE clause
                selectionArgs,                              // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    UserItem contact = null;
                    contact = new UserItem(
                            (int) cursor.getLong(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_USERID)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_JOBTITLE)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_NAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_SURNAME)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_EMAIL)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_COMPANY)),
                            cursor.getString(cursor.getColumnIndex(StudConfContract.ContactEntry.COLUMN_NAME_ISPRESENTER))
                    );
                    if (contact != null) {
                        contacts.add(contact);
                    }
                } while (cursor.moveToNext());
            }
        }
        this.savedContacts = contacts;
    }

    public List<SessionItem> getSavedEvents() {
        return savedEvents;
    }

    public void setSavedEvents(List<SessionItem> savedEvents) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // clear saved data
        dbHelper.emptyPinnedSessions(db);
        // insert new sessions
        for (int i = 0; i < savedEvents.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME, savedEvents.get(i).getStartTime().getTimeInMillis());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME, savedEvents.get(i).getEndTime().getTimeInMillis());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME, savedEvents.get(i).getName());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID, savedEvents.get(i).getCategoryId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID, savedEvents.get(i).getSessionId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_COLOR, savedEvents.get(i).getColor());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION, savedEvents.get(i).getDescription());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID, savedEvents.get(i).getPresenterId());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_NAME, savedEvents.get(i).getPresenterName());
            values.put(StudConfContract.SessionEntry.COLUMN_NAME_SURNAME, savedEvents.get(i).getPresenterSurname());
            db.insert(StudConfContract.SessionEntry.PINNED_TABLE_NAME, "null", values);
        }
        this.savedEvents = savedEvents;
    }

    public MenuItem getSocketStatusItem() {
        return socketStatusItem;
    }

    public SocketHandler getSocketHandler() {
        return socketHandler;
    }

    public String getPrefsName() {
        return PREFS_NAME;
    }

    public NfcHandler getNfcHandler() {
        return nfcHandler;
    }

    public WifiHandler getWifiHandler() {
        return wifiHandler;
    }


    public void updateMenuItemState() {
        if (BuildConfig.FLAVOR.equals("user")) {
            if (getPreferences().isAppActivated()) {
                adapter.enableItem(3);
                adapter.enableItem(4);
            } else {
                adapter.disableItem(3);
                adapter.disableItem(4);
            }
            if (!getPreferences().isNfcAvailable()) {
                adapter.disableItem(1);
            }
        } else {
            if (!getAdminPreferences().isNfcAvailable()) {
                adapter.disableItem(1);
            }
            if (!getAdminPreferences().isAuthenticated()) {
                adapter.disableItem(2);
            } else {
                adapter.enableItem(2);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        Log.d("Activity", "Application started, layout set to activity_main");
        readPreferences();
        readAdminPreferences();

        PackageManager pm = getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_NFC)) {
            onNfcFeatureNotFound();
        } else {
            nfcHandler = new NfcHandler(this);
        }
        dbHelper = new StudConfDBHelper(getBaseContext());
        injectSessions();
        injectContacts();
        injectPinnedSessions();
        wifiHandler = new WifiHandler(this);

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // nav drawer icons from resources
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
        navDrawerItems = new ArrayList<NavDrawerItem>();

        for (int i = 0; i < navMenuTitles.length; i++) {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));
        }

        // Recycle the typed array
        navMenuIcons.recycle();
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        // setting the nav drawer list navMenuIcons
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        updateMenuItemState();
        mDrawerList.setAdapter(adapter);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                toolbar.setTitle(getTitle());
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                toolbar.setTitle(getTitle());
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }

        // create socket only after views are created
        socketHandler = new SocketHandler(this);
        socketHandler.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        socketStatusItem = menu.findItem(R.id.socket_status);
        if (socketHandler.isConnected()) {
            getSocketStatusItem().setIcon(R.drawable.connection_up);
        } else {
            getSocketStatusItem().setIcon(R.drawable.connection_down);
        }
        return true;
    }

    public void setCalendarToNow(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.agendaView)).goToToday();
        try {
            ((WeekView) findViewById(R.id.agendaView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.agendaView)).goToHour(15);
        }
    }

    public void setCalendarToOneDay(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.agendaView)).setNumberOfVisibleDays(1);
        try {
            ((WeekView) findViewById(R.id.agendaView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.agendaView)).goToHour(15);
        }
    }

    public void setCalendarToThreeDays(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.agendaView)).setNumberOfVisibleDays(3);
        try {
            ((WeekView) findViewById(R.id.agendaView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.agendaView)).goToHour(15);
        }
    }

    public void setPinnedToNow(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.savedSessionsView)).goToToday();
        try {
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(15);
        }
    }

    public void setPinnedToOneDay(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.savedSessionsView)).setNumberOfVisibleDays(1);
        try {
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(15);
        }
    }

    public void setPinnedToThreeDays(MenuItem item) {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        ((WeekView) findViewById(R.id.savedSessionsView)).setNumberOfVisibleDays(3);
        try {
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(hour - 1);
        } catch (Exception e) {
            Log.d("Calendar", "Could not set hour to current hour as calendar will not be on screen");
            ((WeekView) findViewById(R.id.savedSessionsView)).goToHour(15);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        // reattach the adapter only if state changed
        if (isRedrawNeeded()) {
            mDrawerList.setAdapter(adapter);
            setRedrawNeeded(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public void displayView(int position) {
        Fragment fragment = null;

        if (BuildConfig.FLAVOR.equals("user")) {
            switch (position) {
                case 0:
                    fragment = new HomeFragment(this);
                    break;
                case 1:
                    fragment = new SetupFragment();
                    break;
                case 2:
                    fragment = new CalendarFragment();
                    break;
                case 3:
                    fragment = new PinnedSessionsFragment();
                    break;
                case 4:
                    fragment = new SavedContactsFragment();
                    break;
                case 5:
                    fragment = new ContactUsFragment();
                    break;
                case 6:
                    finish();
                    System.exit(0);
                    break;
                default:
                    break;
            }
        } else {
            switch (position) {
                case 0:
                    fragment = new HomeFragment(this);
                    break;
                case 1:
                    fragment = new AdminUsersFragment();
                    break;
                case 2:
                    fragment = new AdminWifiFragment();
                    break;
                case 3:
                    finish();
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }


        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "No fragment created, probably disabled item.");
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        toolbar.setTitle(title);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        nfcHandler.processIntent();
    }

    protected void onNfcFeatureNotFound() {
        Toast.makeText(this, "Your phone doens't support NFC. Some features will not work!", Toast.LENGTH_LONG).show();
        getAdminPreferences().setNfcAvailable(false);
        getPreferences().setNfcAvailable(false);
    }

    public void readPreferences() {
        Log.d("Preferences", "Reading preferences from phone memory");
        SharedPreferences settings = getSharedPreferences(getPrefsName(), 0);
        Preferences preferences = new Preferences();
        preferences.loadPreferences(settings);
        setPreferences(preferences);
    }

    public void writePreferences(Preferences preferences) {
        Log.d("Preferences", "Writing preferences to phone memory");
        SharedPreferences settings = getSharedPreferences(getPrefsName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        preferences.savePreferences(editor);
        setPreferences(preferences);
    }

    public void readAdminPreferences() {
        Log.d("Preferences", "Reading ADMIN preferences from phone memory");
        SharedPreferences settings = getSharedPreferences(getPrefsName(), 0);
        AdminPreferences preferences = new AdminPreferences();
        preferences.loadPreferences(settings);
        setAdminPreferences(preferences);
    }

    public void writeAdminPreferences(AdminPreferences preferences) {
        Log.d("Preferences", "Writing ADMIN preferences to phone memory");
        SharedPreferences settings = getSharedPreferences(getPrefsName(), 0);
        SharedPreferences.Editor editor = settings.edit();
        preferences.savePreferences(editor);
        setAdminPreferences(preferences);
    }

    public void switchViewsToActivated(boolean showToast) {
        Log.d("Views", "Switching to Activated Mode");
        findViewById(R.id.linearNotActive).setVisibility(View.GONE);
        findViewById(R.id.linearActive).setVisibility(View.VISIBLE);

        ListView personalDetailsList = (ListView) findViewById(R.id.personalDetailsList);
        ArrayList<String> personalDetails = new ArrayList<String>();
        personalDetails.add(0, getPreferences().getUserItem().getFirstname().concat(" ").concat(getPreferences().getUserItem().getLastname()));
        personalDetails.add(1, getPreferences().getUserItem().getEmail());


        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(this, R.layout.simple_data_row, personalDetails);
        // add my details to the record list on screen
        personalDetailsList.setAdapter(listAdapter);

        ListView wifiDetailsList = (ListView) findViewById(R.id.wifiDetailsList);
        ArrayList<String> wifiDetails = new ArrayList<String>();
        if (BuildConfig.DEBUG) {
            findViewById(R.id.wifiDetailsContainer).setVisibility(View.VISIBLE);
            wifiDetails.add(0, "SSID: " + getPreferences().getNetworkSSID());
            wifiDetails.add(1, "Pass: " + getPreferences().getNetworkPass());
        }

        ArrayAdapter<String> wifiListAdapter = new ArrayAdapter<String>(this, R.layout.simple_data_row, wifiDetails);
        // add my details to the record list on screen
        wifiDetailsList.setAdapter(wifiListAdapter);

        if (showToast) {
            Toast.makeText(this, "Application activated ...", Toast.LENGTH_SHORT).show();
        }
        updateMenuItemState();
    }

    public void switchViewsToDeactivated() {
        Log.d("Views", "Switching to Inactive Mode");
        findViewById(R.id.linearNotActive).setVisibility(View.VISIBLE);
        findViewById(R.id.linearActive).setVisibility(View.GONE);
        updateMenuItemState();
    }

    public void switchViewsToAuth() {
        Log.d("Views", "Switching to Authenticated Mode");
        findViewById(R.id.adminActive).setVisibility(View.VISIBLE);
        findViewById(R.id.adminNotActive).setVisibility(View.GONE);
        updateMenuItemState();
    }

    public void switchViewsToNotAuth() {
        Log.d("Views", "Switching to Anonymous mode");
        findViewById(R.id.adminNotActive).setVisibility(View.VISIBLE);
        findViewById(R.id.adminActive).setVisibility(View.GONE);
        updateMenuItemState();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (nfcHandler != null) {
            nfcHandler.disableForeground();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        socketHandler.connect();
        if (nfcHandler != null) {
            nfcHandler.enableForeground();
         //   nfcHandler.processIntent();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        socketHandler.disconnect();
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            displayView(position);
        }
    }
}
