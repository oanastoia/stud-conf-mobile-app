package oanastoia.studconf;

/**
 * Created by oanastoia on 5/4/15.
 */

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import oanastoia.studconf.model.UserItem;
import oanastoia.studconf.util.NfcHandler;

public class AdminDialogUserFragment extends DialogFragment  {

    private int position;
    private NfcHandler nfcHandler;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle args = getArguments();
        position = args.getInt("position");

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_admin_dialog_participant, new LinearLayout(getActivity()), false);

        // get list of users
        List<UserItem> userItemList = ((MainActivity) getActivity()).getParticipantViewList();
        nfcHandler = ((MainActivity)getActivity()).getNfcHandler();
        // condition required for phones without NFC
        if (nfcHandler != null) {
            nfcHandler.setWriteUserItem(userItemList.get(this.position));
            nfcHandler.enableWriteMode();
        }

        // update view
        ((TextView) view.findViewById(R.id.participantUserId)).setText(Integer.toString(userItemList.get(position).getUserid()));
        ((TextView) view.findViewById(R.id.participantJobTitle)).setText(userItemList.get(position).getJobtitle());
        ((TextView) view.findViewById(R.id.participantFirstName)).setText(userItemList.get(position).getFirstname());
        ((TextView) view.findViewById(R.id.participantLastName)).setText(userItemList.get(position).getLastname());
        ((TextView) view.findViewById(R.id.participantEmail)).setText(userItemList.get(position).getEmail());
        ((TextView) view.findViewById(R.id.participantCompany)).setText(userItemList.get(position).getCompany());

        view.findViewById(R.id.buttonClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // close dialog
                FragmentManager fm = getActivity().getFragmentManager();
                AdminDialogUserFragment dialog = (AdminDialogUserFragment) fm.findFragmentByTag("participantsDialog");
                dialog.dismiss();
            }
        });

        // Build dialog
        Dialog builder = new Dialog(getActivity());
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        builder.setContentView(view);
        return builder;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        nfcHandler.disableWriteMode();
    }
}