package oanastoia.studconf.util;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import oanastoia.studconf.AdminDialogUserFragment;
import oanastoia.studconf.BuildConfig;
import oanastoia.studconf.MainActivity;
import oanastoia.studconf.R;
import oanastoia.studconf.model.UserItem;
import oanastoia.studconf.model.Preferences;

public class NfcHandler {

    private static final String TAG = NfcHandler.class.getSimpleName();
    protected NfcAdapter nfcAdapter;
    protected IntentFilter[] writeTagFilters;
    protected PendingIntent nfcPendingIntent;
    protected boolean writeModeEnabled = false;
    protected boolean foreground = false;
    protected String[] tagMessages;
    protected String[] tagTypeMessages;
    protected MainActivity mainActivity;
    private UserItem writeUserItem;
    private Preferences loadedData;

    public NfcHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;

        nfcAdapter = NfcAdapter.getDefaultAdapter(mainActivity);
        nfcPendingIntent = PendingIntent.getActivity(mainActivity, 0, new Intent(mainActivity, mainActivity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    public Preferences getLoadedData() {
        return loadedData;
    }

    public void setLoadedData(Preferences loadedData) {
        this.loadedData = loadedData;
    }

    public void setWriteUserItem(UserItem writeUserItem) {
        this.writeUserItem = writeUserItem;
    }

    public void enableWriteMode() {
        writeModeEnabled = true;
    }

    public void disableWriteMode() {
        writeModeEnabled = false;
    }


    public void enableForeground() {
        if (!foreground) {
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
            writeTagFilters = new IntentFilter[]{ndefDetected, tagDetected};
            nfcAdapter.enableForegroundDispatch(mainActivity, nfcPendingIntent, writeTagFilters, null);
            foreground = true;
        }
    }

    public void disableForeground() {
        if (foreground) {
            nfcAdapter.disableForegroundDispatch(mainActivity);
            foreground = false;
        }
    }

    public void processIntent() {
        Intent intent = mainActivity.getIntent();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Log.d(TAG, "Process NDEF discovered action");
            nfcIntentDetected(intent, NfcAdapter.ACTION_NDEF_DISCOVERED);
        } else if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Log.d(TAG, "Process TAG discovered action");
            nfcIntentDetected(intent, NfcAdapter.ACTION_TAG_DISCOVERED);
        } else {
            Log.d(TAG, "Ignore action " + intent.getAction());
        }
    }

    private boolean sendDataToTag(Intent intent) {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        if (writeUserItem != null) {

        } else {
            Log.d(TAG, "No data prepared to be written, skipping write...");
            Toast.makeText(mainActivity, "No data found to write on tag", Toast.LENGTH_SHORT).show();
        }

        NdefRecord userid = NdefRecord.createMime("ui", Integer.toString(writeUserItem.getUserid()).getBytes());
        NdefRecord jobtitle = NdefRecord.createMime("jt", writeUserItem.getJobtitle().getBytes());
        NdefRecord firstname = NdefRecord.createMime("fn", writeUserItem.getFirstname().getBytes());
        NdefRecord lastname = NdefRecord.createMime("ln", writeUserItem.getLastname().getBytes());
        NdefRecord email = NdefRecord.createMime("em", writeUserItem.getEmail().getBytes());
        NdefRecord company = NdefRecord.createMime("co", writeUserItem.getCompany().getBytes());
        NdefRecord networkId = NdefRecord.createMime("nid", mainActivity.getAdminPreferences().getNetworkSSID().getBytes());
        NdefRecord networkPassword = NdefRecord.createMime("np", mainActivity.getAdminPreferences().getNetworkPass().getBytes());
        NdefMessage message = new NdefMessage(new NdefRecord[]{userid, jobtitle, firstname, lastname, email, company, networkId, networkPassword});

        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Log.e(TAG, "Tag not writable");
                    return false;
                }
                if (ndef.getMaxSize() < size) {
                    Log.e(TAG, "Tag too small to store your records");
                    return false;
                }
                ndef.writeNdefMessage(message);
                return true;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        return true;
                    } catch (IOException e) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    private boolean readDataFromTag(Intent intent) {
        Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (messages != null) {
            NdefMessage[] ndefMessages = new NdefMessage[messages.length];
            for (int i = 0; i < messages.length; i++) {
                ndefMessages[i] = (NdefMessage) messages[i];
            }
            Log.d(TAG, "Reading NFC data from tag");
            tagMessages = new String[ndefMessages[0].getRecords().length];
            tagTypeMessages = new String[ndefMessages[0].getRecords().length];
            Preferences tagData = new Preferences();
            if (ndefMessages.length > 0) {
                try {
                    for (int j = 0; j < ndefMessages[0].getRecords().length; j++) {
                        tagMessages[j] = new String(ndefMessages[0].getRecords()[j].getPayload());
                        tagTypeMessages[j] = new String(ndefMessages[0].getRecords()[j].getType());
                    }
                    // process data in correct structure
                    for (int j = 0; j < tagMessages.length; j++) {
                        if (tagTypeMessages[j].equals("ui")) {
                            tagData.getUserItem().setUserid(Integer.parseInt(tagMessages[j]));
                        } else if (tagTypeMessages[j].equals("jt")) {
                            tagData.getUserItem().setJobtitle(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("fn")) {
                            tagData.getUserItem().setFirstname(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("ln")) {
                            tagData.getUserItem().setLastname(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("em")) {
                            tagData.getUserItem().setEmail(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("co")) {
                            tagData.getUserItem().setCompany(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("nid")) {
                            tagData.setNetworkSSID(tagMessages[j]);
                        } else if (tagTypeMessages[j].equals("np")) {
                            tagData.setNetworkPass(tagMessages[j]);
                        }
                    }
                    setLoadedData(tagData);
                    return true;
                } catch (Exception e) {
                    Log.d(TAG, "Reading NFC failed, Record Parsing Failure!!");
                }
            }
        }
        return false;
    }

    public void nfcIntentDetected(Intent intent, String action) {
        Log.d(TAG, "ACTION_TAG_DISCOVERED intent detected, checking app mode");
        if (writeModeEnabled) {
            Log.d(TAG, "Write mode detected");
            if (sendDataToTag(intent)) {
                Log.d(TAG, "Tag written successfully");
                // tell main mainActivity write complete
                // Trigger after a successful write. Will close the writing dialog (only place where write is initiated.
                FragmentManager fm = mainActivity.getFragmentManager();
                AdminDialogUserFragment dialog = (AdminDialogUserFragment) fm.findFragmentByTag("participantsDialog");
                Toast.makeText(mainActivity, "NFC tag written ...", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        } else {
            Log.d(TAG, "Read mode detected");
            if (readDataFromTag(intent)) {
                Log.d(TAG, "Tag read successful");
                afterNFCReadTag();
            }
        }
    }

    public void afterNFCReadTag() {
        Fragment fragment = mainActivity.getFragmentManager().findFragmentById(R.id.frame_container);
        // check if we need to setup the application or just save data to saved contacts
        if (BuildConfig.FLAVOR.equals("user")) {
            if (fragment.toString().contains("Setup")) {
                Log.d("NFC", "Active fragment is activation view, check if previously activated");
                if (!mainActivity.getPreferences().isAppActivated()) {
                    Log.d("NFC", "First activation, saving details from NFC tag to SharedPreferences");
                    Preferences preferences = getLoadedData();
                    mainActivity.writePreferences(preferences);
                    mainActivity.switchViewsToActivated(true);
                    mainActivity.setRedrawNeeded(true);
                } else {
                    // check if read NFC is for our activated user
                    if (!(getLoadedData().getUserItem().getUserid() == mainActivity.getPreferences().getUserItem().getUserid())) {
                        Log.d("NFC", "Application previously activate, saving contact instead");
                        mainActivity.getSocketHandler().saveContact(getLoadedData());
                        Toast.makeText(mainActivity, "Saving contact ...", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Toast.makeText(mainActivity, "You can't save your own contact", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                if (!mainActivity.getPreferences().isAppActivated()) {
                    Toast.makeText(mainActivity, "Application not activated yet, please go to Setup to activate it.", Toast.LENGTH_SHORT).show();
                } else {
                    if (!(getLoadedData().getUserItem().getUserid() == mainActivity.getPreferences().getUserItem().getUserid())) {
                        mainActivity.getSocketHandler().saveContact(getLoadedData());
                        Toast.makeText(mainActivity, "Saving contact ...", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mainActivity, "You can't save your own contact", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }
}