package oanastoia.studconf.util;


import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import com.alamkanak.weekview.WeekView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import oanastoia.studconf.BuildConfig;
import oanastoia.studconf.DialogContactFragment;
import oanastoia.studconf.MainActivity;
import oanastoia.studconf.AdminDialogUserFragment;
import oanastoia.studconf.R;
import oanastoia.studconf.adapter.ContactListAdapter;
import oanastoia.studconf.model.AdminPreferences;
import oanastoia.studconf.model.Preferences;
import oanastoia.studconf.model.SessionItem;
import oanastoia.studconf.model.UserItem;

/**
 * Created by oanastoia on 4/28/15.
 */
public class SocketHandler {
    protected String connectionString;
    protected MainActivity mainActivity;
    Socket socketio;
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            int status = R.drawable.connection_down;
            updateConnectionStatus(status);
        }
    };
    private Emitter.Listener onConnectSuccess = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            int status = R.drawable.connection_up;
            updateConnectionStatus(status);
        }
    };
    private Emitter.Listener onReconnectSuccess = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            int status = R.drawable.connection_up;
            updateConnectionStatus(status);
            if (BuildConfig.FLAVOR.equals("user")) {
                retrieveCalendar();
                if (mainActivity.getPreferences().isAppActivated() == true) {
                    retrievePinnedSessions((Integer) args[0]);
                    retrieveSavedContacts();
                }
            }
            else {
                if (mainActivity.getAdminPreferences().isAuthenticated() == true) {
                    retrieveUsers(mainActivity.getAdminPreferences().getToken());
                    retrieveWifiDetails(mainActivity.getAdminPreferences().getToken());
                }
            }
        }
    };
    private Emitter.Listener onUserSessions = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mainActivity.setSavedEvents(parseSessions((JSONArray) args[0]));
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        WeekView mWeekView = (WeekView) mainActivity.findViewById(R.id.savedSessionsView);
                        mWeekView.notifyDatasetChanged();
                    } catch (Exception e) {
                        Log.d("Socket", "Tried to update My Calendar view, but Calendar not yet present");
                    }
                }
            });
        }
    };
    private Emitter.Listener onSessions = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d("Socket", "Calendar received");
            // set global events
            mainActivity.setEvents(parseSessions((JSONArray) args[0]));
            // trigger calendar update
            updateCalendar();
        }
    };
    private Emitter.Listener onAdminUsers = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // put contacts in a global object to use in the view
            mainActivity.setParticipantViewList(parseUsersList((JSONArray) args[0]));
            updateAdminUsersList();
        }
    };

    private Emitter.Listener onAdminWifiDetails = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // put contacts in a global object to use in the view
            AdminPreferences preferences = mainActivity.getAdminPreferences();
            try {
                preferences.setNetworkSSID(((JSONObject) args[0]).getString("networkSSID"));
                preferences.setNetworkPass(((JSONObject) args[0]).getString("networkPass"));
                updateAdminWifiDetails();
            } catch(JSONException e) {
                Log.e("Socket", "Could not read Wifi Details");
            }
            mainActivity.writeAdminPreferences(preferences);
        }
    };

    private Emitter.Listener onUserContacts = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            mainActivity.setSavedContacts(parseUsersList((JSONArray) args[0]));
            updateUserContacts();
        }
    };
    private Emitter.Listener onAdminSessionExpired = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // invalidate any token on the device (if getting signal in middle of flow)
            AdminPreferences preferences = mainActivity.getAdminPreferences();
            preferences.setToken("");
            mainActivity.writeAdminPreferences(preferences);
            Log.d("PREF", "Token erased from device");
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(mainActivity, "Your session has expired ...", Toast.LENGTH_SHORT).show();
                        // switch to Admin
                        mainActivity.displayView(1);
                        mainActivity.switchViewsToNotAuth();
                    } catch (Exception e) {
                        Log.d("Socket", "Login failed, but couldn't show toast message");
                    }

                }
            });

        }
    };
    private Emitter.Listener onAdminLoginFailed = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            // invalidate any token on the device (if getting signal in middle of flow)
            AdminPreferences preferences = mainActivity.getAdminPreferences();
            preferences.setToken("");
            mainActivity.writeAdminPreferences(preferences);
            Log.d("PREF", "Token erased from device");
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(mainActivity, "Invalid credentials ...", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.d("Socket", "Login failed, but couldn't show toast message");
                    }
                }
            });
        }
    };
    private Emitter.Listener onAdminLogin = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            AdminPreferences preferences = mainActivity.getAdminPreferences();
            preferences.setToken(args[0].toString());
            mainActivity.writeAdminPreferences(preferences);
            mainActivity.setRedrawNeeded(true);
            retrieveUsers(mainActivity.getAdminPreferences().getToken());
            retrieveWifiDetails(mainActivity.getAdminPreferences().getToken());
            Log.d("PREF", "Saved token to the device, holding session between application sessions");
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mainActivity.switchViewsToAuth();
                    } catch (Exception e) {
                        Log.d("Socket", "Connection successful, but view not ready to update status");
                    }
                }
            });
        }
    };
    private Emitter.Listener onUserSessionsInvalid = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            retrievePinnedSessions(0);
        }
    };
    public SocketHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.connectionString = "http://" + mainActivity.getPreferences().getServerHost() + ":" + mainActivity.getPreferences().getServerPort() + "/";
    }

    public void connect() {
        try {
            socketio = IO.socket(connectionString);
        } catch (URISyntaxException e) {
            Log.d("Socket", "Failed to set connection string: ", e);
        }

        // System events for Socket connection handling
        socketio.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        socketio.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        socketio.on(Socket.EVENT_DISCONNECT, onConnectError);
        socketio.on(Socket.EVENT_CONNECT, onConnectSuccess);
        socketio.on(Socket.EVENT_RECONNECT, onReconnectSuccess);

        // Public events (calendar, online users, news, etc.)
        socketio.on("public:sessions", onSessions);

        // User specific events
        socketio.on("user:sessions", onUserSessions);
        socketio.on("user:contacts", onUserContacts);

        // Admin specific events
        socketio.on("admin:login", onAdminLogin);
        socketio.on("admin:login-failed", onAdminLoginFailed);
        socketio.on("admin:session-expired", onAdminSessionExpired);
        socketio.on("admin:users", onAdminUsers);
        socketio.on("admin:wifi", onAdminWifiDetails);
        socketio.on("user:sessions-pinend-invalid", onUserSessionsInvalid);

        try {
            socketio.connect();
        } catch (Exception e) {
            Log.d("Socket", "Exception while connecting: ", e);
        }
    }

    public boolean isConnected() {
        return socketio.connected();
    }

    public void disconnect() {
        socketio.disconnect();
    }

    public void retrieveCalendar() {
        emit("public:sessions");
    }

    public void deleteContact(int contactid) {
        emit("user:contact-delete", "{ " +
                "'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() + "'" +
                ", 'contactid': '" + Integer.toString(contactid) +
                "' }");
    }

    public void retrievePinnedSessions(int retry) {
        emit("user:sessions", "{ " +
                "'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() +
                "', 'retry': '" + Integer.toString(retry) + // used for debugging reconnects
            "' }");
    }

    public void login(String username, String password) {
        emit("admin:login", "{ 'user' : '" + username + "', " +
                "'pass' : '" + password + "' }");
    }

    public void logout(String token) {
        emit("admin:logout", "{ 'token' : '" + token + "' }");
    }

    public void retrieveUsers(String token) {
        emit("admin:users", "{ 'token' : '" + token + "' }");
    }

    public void retrieveWifiDetails(String token) {
        emit("admin:wifi", "{ 'token' : '" + token + "' }");
    }

    public void retrieveSavedContacts() {
        emit("user:contacts", "{ 'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() +
                "' }");
        retrieveCalendar();
    }

    public void saveContact(Preferences readNFCItem) {
        emit("user:contact-save", "{ " +
                "'participantid' : '" + readNFCItem.getUserItem().getUserid() + "'," +
                "'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() +
                "'}");
    }

    public void pinSession(int sessionId) {
        emit("user:session-pin", "{ " +
                "'sessionid' : '" + Integer.toString(sessionId) + "', " +
                "'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() +
                "' }");
    }

    public void unpinSession(int sessionId) {
        emit("user:session-unpin", "{ " +
                "'sessionid' : '" + Integer.toString(sessionId) + "', " +
                "'ownerid' : '" + mainActivity.getPreferences().getUserItem().getUserid() +
                "' }");
    }

    // TODO: Move this to MainActivity?
    private void updateConnectionStatus(final int status) {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    mainActivity.getSocketStatusItem().setIcon(status);
                } catch (Exception e) {
                    Log.d("Socket", "No connection, but view not ready to show status");
                }

            }
        });
    }

    // TODO: Move this to CalendarFragment
    private void updateCalendar() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    WeekView mWeekView = (WeekView) mainActivity.findViewById(R.id.agendaView);
                    mWeekView.notifyDatasetChanged();
                } catch (Exception e) {
                    Log.d("Socket", "Tried to update Calendar view, but Calendar not yet present");
                }
            }
        });
    }

    // TODO: Move this to SavedContactsFragment
    private void updateUserContacts() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<UserItem> savedContacts = mainActivity.getSavedContacts();
                    ListView mainListView = (ListView) mainActivity.findViewById(R.id.saveContactsListView);
                    ContactListAdapter listAdapter = new ContactListAdapter(mainActivity.getApplicationContext(), savedContacts);
                    // add my details to the record list on screen
                    mainListView.setAdapter(listAdapter);
                    mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            FragmentManager fm = mainActivity.getFragmentManager();
                            DialogContactFragment dialog = new DialogContactFragment();
                            Bundle args = new Bundle();
                            args.putInt("position", position);
                            dialog.setArguments(args);
                            dialog.show(fm, "contactDialog");
                        }
                    });
                } catch (Exception e) {
                    Log.d("Socket", "Tried to update Saved Contacts view, but Saved Contacts view not yet present");
                }
            }
        });
    }

    // TODO: Move this to AdminUsersFragment
    private void updateAdminUsersList() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ArrayList<UserItem> participantsViewList;
                    participantsViewList = mainActivity.getParticipantViewList();
                    ListView mainListView = (ListView) mainActivity.findViewById(R.id.adminUsersViewList);

                    ContactListAdapter listAdapter = new ContactListAdapter(mainActivity.getApplicationContext(), participantsViewList);
                    // add my details to the record list on screen
                    mainListView.setAdapter(listAdapter);
                    mainListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                            FragmentManager fm = mainActivity.getFragmentManager();
                            AdminDialogUserFragment dialog = new AdminDialogUserFragment();
                            Bundle args = new Bundle();
                            args.putInt("position", position);
                            dialog.setArguments(args);
                            dialog.show(fm, "participantsDialog");
                            return true;
                        }
                    });

                } catch (Exception e) {
                    Log.d("Socket", "Tried to update Saved Contacts view, but Saved Contacts view not yet present");
                }
            }
        });
    }

    private void updateAdminWifiDetails() {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ListView wifiDetailsList = (ListView) mainActivity.findViewById(R.id.adminWifiList);
                    ArrayList<String> wifiDetails = new ArrayList<String>();
                    wifiDetails.add(0, mainActivity.getAdminPreferences().getNetworkSSID());
                    wifiDetails.add(1, mainActivity.getAdminPreferences().getNetworkPass());
                    ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(mainActivity, R.layout.simple_data_row, wifiDetails);
                    wifiDetailsList.setAdapter(listAdapter);

                } catch (Exception e) {
                    Log.d("Socket", "Tried to update Saved Contacts view, but Saved Contacts view not yet present");
                }
            }
        });
    }


    private ArrayList<SessionItem> parseSessions(JSONArray data) {

        ArrayList<SessionItem> events = new ArrayList<SessionItem>();
        for (int i = 0; i < data.length(); i++) {
            SessionItem event = null;
            try {
                Calendar startTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                startTime.setTimeInMillis((Integer) data.getJSONObject(i).get("STARTTIME") * 1000L);
                Calendar endTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                endTime.setTimeInMillis((Integer) data.getJSONObject(i).get("ENDTIME") * 1000L);
                int color;
                switch ((Integer) data.getJSONObject(i).get("CATEGORYID")) {
                    case 1:
                        color = mainActivity.getResources().getColor(R.color.event_color_01);
                        break;
                    case 2:
                        color = mainActivity.getResources().getColor(R.color.event_color_02);
                        break;
                    case 3:
                        color = mainActivity.getResources().getColor(R.color.event_color_03);
                        break;
                    case 4:
                        color = mainActivity.getResources().getColor(R.color.event_color_04);
                        break;
                    default:
                        color = mainActivity.getResources().getColor(R.color.event_color_01);
                        break;
                }

                event = new SessionItem(i, (String) data.getJSONObject(i).get("SESSIONNAME"), (String) data.getJSONObject(i).get("DESCRIPTION"), startTime, endTime, color, (Integer) data.getJSONObject(i).get("SESSIONID"), (Integer) data.getJSONObject(i).get("PRESENTERID"), (Integer) data.getJSONObject(i).get("CATEGORYID"), (String) data.getJSONObject(i).get("NAME"), (String) data.getJSONObject(i).get("SURNAME"));

            } catch (Exception e) {
                Log.d("Socket", "Could not parse data for calendar", e);
            }
            if (event != null) {
                events.add(event);
            }
        }
        return events;
    }

    private ArrayList<UserItem> parseUsersList(JSONArray json) {
        ArrayList<UserItem> usersList = new ArrayList<UserItem>();
        for (int i = 0; i < json.length(); i++) {
            UserItem user = new UserItem();
            try {
                user.setUserid((Integer) json.getJSONObject(i).get("USERID"));
                user.setJobtitle((String) json.getJSONObject(i).get("JOBTITLE"));
                user.setFirstname((String) json.getJSONObject(i).get("NAME"));
                user.setLastname((String) json.getJSONObject(i).get("SURNAME"));
                user.setEmail((String) json.getJSONObject(i).get("EMAIL"));
                user.setCompany((String) json.getJSONObject(i).get("COMPANY"));
                user.setIspresenter(json.getJSONObject(i).get("ISPRESENTER").toString());
            } catch (JSONException e) {
                Log.d("Socket", "Unable to parse json from server ", e);
            }
            usersList.add(user);
        }
        return usersList;
    }

    private void emit(String event){
        Log.d("Socket", "Emit " + event);
        socketio.emit(event, new JSONObject());
    }

    private void emit(String event, String payload){
        Log.d("Socket", "Emit " + event);
        JSONObject data;
        try {
            data = new JSONObject(payload);
            socketio.emit(event, data);
        } catch (Exception e) {
            Log.d("Socket", "Error building payload for " + event + ". Preparing empty object");
            data = new JSONObject();
            socketio.emit(event, data);
        }
    }
}