package oanastoia.studconf.util;

/**
 * Created by oanastoia on 4/26/15.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import oanastoia.studconf.model.StudConfContract;

public class StudConfDBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "StudConf.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_SESSIONS =
            "CREATE TABLE " + StudConfContract.SessionEntry.TABLE_NAME + " (" +
                    StudConfContract.SessionEntry._ID + " INTEGER PRIMARY KEY," +
                    StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_COLOR + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SURNAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID + INT_TYPE +
                    " )";
    private static final String SQL_CREATE_PINNED_SESSIONS =
            "CREATE TABLE " + StudConfContract.SessionEntry.PINNED_TABLE_NAME + " (" +
                    StudConfContract.SessionEntry._ID + " INTEGER PRIMARY KEY," +
                    StudConfContract.SessionEntry.COLUMN_NAME_STARTTIME + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_ENDTIME + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SESSIONNAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_CATEGORYID + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SESSIONID + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_COLOR + INT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_SURNAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.SessionEntry.COLUMN_NAME_PRESENTERID + INT_TYPE +
                    " )";
    private static final String SQL_CREATE_CONTACTS =
            "CREATE TABLE " + StudConfContract.ContactEntry.TABLE_NAME + " (" +
                    StudConfContract.ContactEntry._ID + " INTEGER PRIMARY KEY," +
                    StudConfContract.ContactEntry.COLUMN_NAME_JOBTITLE + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_SURNAME + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_EMAIL + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_COMPANY + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_ISPRESENTER + TEXT_TYPE + COMMA_SEP +
                    StudConfContract.ContactEntry.COLUMN_NAME_USERID + INT_TYPE +
                    " )";

    private static final String SQL_DELETE_SESSIONS =
            "DROP TABLE IF EXISTS " + StudConfContract.SessionEntry.TABLE_NAME;

    private static final String SQL_DELETE_PINNED_SESSIONS =
            "DROP TABLE IF EXISTS " + StudConfContract.SessionEntry.PINNED_TABLE_NAME;

    private static final String SQL_DELETE_CONTACTS =
            "DROP TABLE IF EXISTS " + StudConfContract.ContactEntry.TABLE_NAME;

    private static final String SQL_EMPTY_SESSIONS =
            "DELETE FROM " + StudConfContract.SessionEntry.TABLE_NAME + " WHERE 1";

    private static final String SQL_EMPTY_PINNED_SESSIONS =
            "DELETE FROM " + StudConfContract.SessionEntry.PINNED_TABLE_NAME + " WHERE 1";

    private static final String SQL_EMPTY_CONTACTS =
            "DELETE FROM " + StudConfContract.ContactEntry.TABLE_NAME + " WHERE 1";

    public StudConfDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_SESSIONS);
        db.execSQL(SQL_CREATE_PINNED_SESSIONS);
        db.execSQL(SQL_CREATE_CONTACTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_SESSIONS);
        db.execSQL(SQL_DELETE_PINNED_SESSIONS);
        db.execSQL(SQL_DELETE_CONTACTS);
        onCreate(db);
    }
    public void emptyPinnedSessions(SQLiteDatabase db) {
        db.execSQL(SQL_EMPTY_PINNED_SESSIONS);
    }
    public void emptySessions(SQLiteDatabase db) {
        db.execSQL(SQL_EMPTY_SESSIONS);
    }
    public void emptyContacts(SQLiteDatabase db) {
        db.execSQL(SQL_EMPTY_CONTACTS);
    }
}
