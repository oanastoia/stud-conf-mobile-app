package oanastoia.studconf.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import oanastoia.studconf.MainActivity;

/**
 * Created by marius on 28/04/15.
 */
public class WifiHandler {
    private MainActivity mainActivity;
    private WifiEnableReceiver enableReceiver;
    private WifiConnectReceiver connectionReceiver;
    private static final ScheduledExecutorService worker = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture taskHandler;

    public WifiHandler(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    private int getSecondsTimeout() {
        return 30; // number of seconds until we consider the connection to wifi failed
    }

    private class WifiEnableReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("WIFI", "Intent triggered - " + intent.getAction());
            if (intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN) == WifiManager.WIFI_STATE_ENABLED) {
                mainActivity.unregisterReceiver(this);
                Log.d("WIFI", "Enabled Wifi");
                configureWifi();

            } else {
                Log.d("WIFI", "Wifi enabling receiver triggered, but state is not yet enabled");
            }
        }
    }


    private class WifiConnectReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("WIFI", "Intent triggered - " + intent.getAction());
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo.isConnected()) {
                Toast.makeText(mainActivity, "You are now connected to the conference WiFi network", Toast.LENGTH_LONG).show();
                Log.d("WIFI", "Connected!");
                mainActivity.getSocketHandler().connect();
                mainActivity.unregisterReceiver(this);
                if (taskHandler != null) {
                    // cancel the timeout task
                    taskHandler.cancel(true);
                }
            } else {
                Log.i("WIFI", "Not connected yet, unknown flow");
            }
        }
    }

    public void setupWifi() {
        WifiManager wifi = (WifiManager) mainActivity.getSystemService(Context.WIFI_SERVICE);
        // check if wifi is enabled using wifi service from Android
        if (!wifi.isWifiEnabled()) {
            Toast.makeText(mainActivity, "Your Wifi is currently disabled, re-enabling Wifi ...", Toast.LENGTH_SHORT).show();
            enableReceiver = new WifiEnableReceiver();
            mainActivity.registerReceiver(enableReceiver, new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
            wifi.setWifiEnabled(true);
        } else {
            Log.d("WIFI", "Wifi already enabled, configuring");
            configureWifi();
        }
    }

    public void configureWifi() {
        Toast.makeText(mainActivity, "Connecting you to the conference WiFi ...", Toast.LENGTH_LONG).show();
        WifiManager wifi = (WifiManager) mainActivity.getSystemService(Context.WIFI_SERVICE);
        String networkSSID = mainActivity.getPreferences().getNetworkSSID();
        String networkPass = mainActivity.getPreferences().getNetworkPass();
        boolean existingNetwork = false;

        WifiConfiguration conf = new WifiConfiguration();
        // assuming WPA network setup (TODO: do this for generic wifi based on settings)
        conf.SSID = "\"".concat(networkSSID).concat("\"");
        conf.preSharedKey = "\"".concat(networkPass).concat("\"");
        conf.status = WifiConfiguration.Status.ENABLED;

        // try to search for current network if it exists to update config
        List<WifiConfiguration> list = wifi.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                conf.networkId = i.networkId;
                existingNetwork = true;
                break;
            }
        }
        if (existingNetwork) {
            Log.d("WIFI", "Network already exists, updating settings");
            wifi.updateNetwork(conf);
        } else {
            Log.d("WIFI", "New network, adding settings");
            wifi.addNetwork(conf);
        }
        list = wifi.getConfiguredNetworks();
        for (WifiConfiguration i : list) {
            if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                Log.d("WIFI", "Force a reconnect for " + networkSSID);
                taskHandler = worker.schedule(new TimeoutTask(), getSecondsTimeout(), TimeUnit.SECONDS);
                connectionReceiver = new WifiConnectReceiver();
                mainActivity.registerReceiver(connectionReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                wifi.disconnect();
                wifi.enableNetwork(i.networkId, true);
                wifi.reconnect();
                break;
            }
        }
    }

    private class TimeoutTask implements Runnable {
        @Override
        public void run() {
            // called when we reach the timeout
            mainActivity.unregisterReceiver(connectionReceiver);
            // runOnUiThread needed to update things on the interface (Showing a dialog)
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new AlertDialog.Builder(mainActivity)
                            .setCancelable(false)
                            .setMessage(String.format("Sorry, you were unable to connect your phone to the conference Wifi.\n" +
                                    "Possible causes:\n\n" +
                                    " - You are not in range of our routers\n" +
                                    " - There is a problem with your conference ticket (NFC tag)\n\n" +
                                    "Please contact the conference personnel if the problem persists."))
                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            });
        }
    }
}