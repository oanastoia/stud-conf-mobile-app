package oanastoia.studconf;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class HomeFragment extends Fragment {

    MainActivity mainActivity;

	public HomeFragment(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        // TODO: Create home grid dynamically
        rootView.findViewById(R.id.hButtonActivation).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).displayView(1);
            }
        });

        rootView.findViewById(R.id.hButtonCalendar).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).displayView(2);
            }
        });

        rootView.findViewById(R.id.hButtonSavedSessions).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).getPreferences().isAppActivated()) {
                    ((MainActivity) getActivity()).displayView(3);
                } else {
                    Toast.makeText(getActivity(), "Please go to `Setup` and activate the application", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rootView.findViewById(R.id.hButtonSavedContacts).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).getPreferences().isAppActivated()) {
                    ((MainActivity) getActivity()).displayView(4);
                } else {
                    Toast.makeText(getActivity(), "Please go to `Setup` and activate the application", Toast.LENGTH_SHORT).show();
                }
            }
        });


        rootView.findViewById(R.id.hButtonContact).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).displayView(5);
            }
        });

        rootView.findViewById(R.id.hButtonExit).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().finish();
                System.exit(0);
            }
        });

        rootView.findViewById(R.id.hButtonAdminUsersList).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Display view 1 as this will only be called on Admin app
                ((MainActivity) getActivity()).displayView(1);
            }
        });

        rootView.findViewById(R.id.hButtonAdminWifiDetails).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (((MainActivity) getActivity()).getAdminPreferences().isAuthenticated()) {
                    ((MainActivity) getActivity()).displayView(2);
                } else {
                    ((MainActivity) getActivity()).displayView(1);
                }

            }
        });


        return rootView;
    }
}
