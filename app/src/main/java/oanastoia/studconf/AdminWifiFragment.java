package oanastoia.studconf;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import oanastoia.studconf.model.AdminPreferences;
import oanastoia.studconf.util.SocketHandler;

public class AdminWifiFragment extends Fragment {

	public AdminWifiFragment(){}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin_wifi, container, false);
        SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
        if (((MainActivity) getActivity()).getAdminPreferences().isAuthenticated()) {
            socketHandler.retrieveWifiDetails(((MainActivity) getActivity()).getAdminPreferences().getToken());
        }
        ListView wifiDetailsList = (ListView) rootView.findViewById(R.id.adminWifiList);
        ArrayList<String> wifiDetails = new ArrayList<String>();
        wifiDetails.add(0, ((MainActivity) getActivity()).getAdminPreferences().getNetworkSSID());
        wifiDetails.add(1, ((MainActivity) getActivity()).getAdminPreferences().getNetworkPass());
        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_data_row, wifiDetails);
        wifiDetailsList.setAdapter(listAdapter);

        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
