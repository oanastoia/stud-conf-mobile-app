package oanastoia.studconf.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

import oanastoia.studconf.R;
import oanastoia.studconf.model.SessionItem;

public class SessionsListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<SessionItem>sessionItem;

    public SessionsListAdapter(Context context, ArrayList<SessionItem> sessionItem) {
        this.context = context;
        this.sessionItem = sessionItem;
    }

    @Override
    public int getCount() {
        try {
            return sessionItem.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return sessionItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.session_data_row, null);
        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.text);
        txtTitle.setText(sessionItem.get(position).getName());
        TextView txtSubTitle = (TextView) convertView.findViewById(R.id.subText);

        Calendar startTime = sessionItem.get(position).getStartTime();
        Calendar endTime = sessionItem.get(position).getEndTime();
        String textDate = getMonthForInt(startTime.get(Calendar.MONTH)).concat(" ").concat(Integer.toString(startTime.get(Calendar.DAY_OF_MONTH)));
        String textStartTime = String.format("%02d", startTime.get(Calendar.HOUR_OF_DAY)).concat(":").concat(String.format("%02d", startTime.get(Calendar.MINUTE)));
        String textEndTime = String.format("%02d", endTime.get(Calendar.HOUR_OF_DAY)).concat(":").concat(String.format("%02d", endTime.get(Calendar.MINUTE)));
        txtSubTitle.setText(textDate.concat(" : ").concat(textStartTime).concat(" - ").concat(textEndTime));
        TextView colorBadge = (TextView) convertView.findViewById(R.id.badge);

        switch (sessionItem.get(position).getCategoryId()) {
            case 1:
                colorBadge.setBackground(context.getResources().getDrawable(R.drawable.session_badge_01));
                break;
            case 2:
                colorBadge.setBackground(context.getResources().getDrawable(R.drawable.session_badge_02));
                break;
            case 3:
                colorBadge.setBackground(context.getResources().getDrawable(R.drawable.session_badge_03));
                break;
            case 4:
                colorBadge.setBackground(context.getResources().getDrawable(R.drawable.session_badge_04));
                break;
            default:
                colorBadge.setBackground(context.getResources().getDrawable(R.drawable.session_badge_01));
                break;
        }

        return convertView;
    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11 ) {
            month = months[num];
        }
        return month;
    }
}