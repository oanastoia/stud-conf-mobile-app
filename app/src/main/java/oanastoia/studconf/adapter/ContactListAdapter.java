package oanastoia.studconf.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import oanastoia.studconf.R;
import oanastoia.studconf.model.UserItem;

public class ContactListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<UserItem>contactItems;

    public ContactListAdapter(Context context, ArrayList<UserItem> contactItems) {
        this.context = context;
        this.contactItems = contactItems;
    }

    @Override
    public int getCount() {
        return contactItems.size();
    }

    @Override
    public Object getItem(int position) {
        return contactItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.contact_data_row, null);
        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.text);
        txtTitle.setText(contactItems.get(position).getContactsText());

        TextView txtBadge = (TextView) convertView.findViewById(R.id.badge);
        txtBadge.setText(contactItems.get(position).getInitials());

        return convertView;
    }

}
