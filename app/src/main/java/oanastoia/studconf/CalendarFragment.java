package oanastoia.studconf;

import android.app.Fragment;

import android.app.FragmentManager;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.List;

import oanastoia.studconf.model.SessionItem;
import oanastoia.studconf.util.SocketHandler;

public class CalendarFragment extends Fragment implements WeekView.MonthChangeListener, WeekView.EventClickListener, WeekView.EventLongPressListener {

    private WeekView mWeekView;

    public CalendarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SocketHandler socketHandler = ((MainActivity) getActivity()).getSocketHandler();
        socketHandler.retrieveCalendar();

        final View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        mWeekView = (WeekView) rootView.findViewById(R.id.agendaView);
        mWeekView.setOnEventClickListener(this);
        mWeekView.setMonthChangeListener(this);
        mWeekView.setEventLongPressListener(this);
        mWeekView.goToToday();
        mWeekView.goToHour(9);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
        List<SessionItem> sessionItems = ((MainActivity) getActivity()).getEvents();
        try {
            for (int i = 0; i < sessionItems.size(); i++) {
                if (sessionItems.get(i).getStartTime().getTime().getMonth()+1 == newMonth) {
                    events.add(sessionItems.get(i));
                }
            }
        } catch (Exception e) {
            Log.d("Calendar", "No events stored yet in the application");
        }
        return events;
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        // get reference to cached events
        List<SessionItem> cachedEvents = ((MainActivity) getActivity()).getEvents();
        FragmentManager fm = getActivity().getFragmentManager();
        DialogSessionFragment dialog = new DialogSessionFragment(cachedEvents.get((int) event.getId()), false);
        dialog.show(fm, "eventDetailsDialog");
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {
        // empty
    }

    @Override
    public void onCreateOptionsMenu( Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.calendar_menu, menu);
    }
}
