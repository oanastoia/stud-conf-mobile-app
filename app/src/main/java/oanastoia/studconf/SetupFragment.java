package oanastoia.studconf;

import android.app.Fragment;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import oanastoia.studconf.model.Preferences;
import oanastoia.studconf.model.SessionItem;
import oanastoia.studconf.model.UserItem;
import oanastoia.studconf.util.WifiHandler;

public class SetupFragment extends Fragment {
    WifiHandler wifiHandler;

    public SetupFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        wifiHandler = ((MainActivity) getActivity()).getWifiHandler();
        View rootView = inflater.inflate(R.layout.fragment_setup, container, false);

        rootView.findViewById(R.id.EnableWifi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("WIFI", "User clicked setup wifi button");
                wifiHandler.setupWifi();
            }
        });

        rootView.findViewById(R.id.deactivationButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // clear preferences data
                Preferences preferences = new Preferences();
                ((MainActivity) getActivity()).writePreferences(preferences);
                ((MainActivity) getActivity()).switchViewsToDeactivated();
                ((MainActivity) getActivity()).setRedrawNeeded(true);

                // switch database to write mode
                SQLiteDatabase db = ((MainActivity) getActivity()).getDbHelper().getWritableDatabase();
                // run empty sepecific user details from the database
                ((MainActivity) getActivity()).getDbHelper().emptyPinnedSessions(db);
                ((MainActivity) getActivity()).getDbHelper().emptyContacts(db);
                // set objects in applications to empty
                ((MainActivity) getActivity()).setSavedContacts(new ArrayList<UserItem>());
                ((MainActivity) getActivity()).setSavedEvents(new ArrayList<SessionItem>());

                Toast.makeText(getActivity(), "Application deactivated, read your tag again to activate!", Toast.LENGTH_LONG).show();
            }
        });

        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!((MainActivity) getActivity()).getPreferences().isAppActivated()) {
            ((MainActivity) getActivity()).switchViewsToDeactivated();
        }
        else {
            ((MainActivity) getActivity()).switchViewsToActivated(false);
        }
    }
}
